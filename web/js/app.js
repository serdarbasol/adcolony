(function() {
  let Tilt = function() {

    // INITIAL VALUES
    let
      welcomeModalTime = 4000,
      maxSpeed         = 5,
      speed            = 0,
      pos              = 0
    ;

    let
      maxpos,
      replay,
      recipies,
      fullWidth,
      overlayClosed = false;
    ;
    let body = document.body;

    const _preInit = function() {
      var orientationHandlerScreenType = function() {
        if (window.orientation === 90 || window.orientation === -90) {
          body.className = "landscape overlayIsOpen hidden";
          Tilt.init();
        }
        else if (window.orientation === 0 || window.orientation === -180) {
          body.className = "portrait";
          alert("Need portrait mode!")
        }
        else if (window.orientation === undefined) {
          body.className = "none";
        }
        else {
          alert("Unknown orientation")
        }
      }
      window.addEventListener('orientationchange', orientationHandlerScreenType );
      orientationHandlerScreenType();
    };

    const _init = function() {

      if(!window.DeviceOrientationEvent) {
        alert("Device is not supprted!");
        return false;
      }
      recipies = document.querySelectorAll(".recipies");
      replay   = document.getElementById('replay');

      _addEventHandlers();
      _addCloseOverlayListener();
      _pulseRecipies();
      _scrollWin();
      _addReplayListener();
      _addModalClickListener();
    };

    const _addEventHandlers = function() {
      window.addEventListener('deviceorientation', (e)=> _addTiltListener(e));
      replay.addEventListener('click', _addReplayListener);
    };

    const _addCloseOverlayListener = function() {
      setTimeout(()=> {
        pos                     = 0;
        speed                   = 0;
        overlayClosed           = true;
        body.className = "landscape";
        _pulseRecipies();
      },  welcomeModalTime);
    };

    const _addTiltListener = function(event) {
      fullWidth = document.getElementsByClassName('bg')[0].width;
      maxpos    = fullWidth -  window.innerWidth;
      let alpha = parseInt(event.alpha);
      let beta  = parseInt(event.beta);
      let gamma = parseInt(event.gamma);
      _pulseRecipies();

      if (beta > 10) {speed = maxSpeed}
      else if (beta < -10) {speed = -maxSpeed}
      else {speed = beta / 2};
      // dataContainerOrientation.innerHTML = 'speed ' + speed + '<br/>pos: ' + pos;
      _scrollWin();
    };

    const _scrollWin = function() {
      pos = pos + speed;
      if(pos < 0) pos = 0
      if(pos > maxpos) pos = maxpos;
      window.scrollTo(pos, 0);
    };

    const _pulseRecipies = function() {
      if(!overlayClosed) return false;
      recipies.forEach((item, index) => {
        if(item.offsetLeft < window.innerWidth + pos + 633) {
          item.className = item.className + ' animated pulse';
          recipies = [].slice.call(recipies, index);
        }
      })
    };

    const _addReplayListener = function() {
      body.className = "landscape";
      body.className = "landscape overlayIsOpen hidden";
      recipies = document.querySelectorAll(".recipies");
      recipies.forEach((item, index) => {
        item.classList.remove("animated");
        item.classList.remove("pulse");
      });

      _addCloseOverlayListener();
    };

    const _addModalClickListener = function() {
      let overlay = document.getElementsByClassName('overlay')[0],
        overlayBtts = document.querySelectorAll('div[class*="overlay"]');

      [].forEach.call(overlayBtts, function(btt) {

        btt.addEventListener('click', function(event) {
          let recipeType = event.target.getAttribute('recipe-type');
          let overlay = document.getElementsByClassName('overlay')[0];
          let overlayHold = document.getElementsByClassName('overlay-background-holder')[0];

          while (overlayHold.firstChild) {
            overlayHold.removeChild(overlayHold.firstChild);
          }

          let detailModalDiv       = document.createElement("div");
          detailModalDiv.className = `detail-${recipeType}`;
          overlayHold.appendChild(detailModalDiv);

          let overlayOpen = [...this.classList].includes('open-overlay');
          overlay.setAttribute('aria-hidden', !overlayOpen);

          setTimeout(function() {overlay.scrollTop = 0;}, 200)
          overlayOpen ? body.classList.add("hidden") : body.classList.remove("hidden");
          speed = 0;
        }, false);

      });

    };
    return {
      preInit: _preInit,
      init: _init,
    }
  }();

  Tilt.preInit();
})();